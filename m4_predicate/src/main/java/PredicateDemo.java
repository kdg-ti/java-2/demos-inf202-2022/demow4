import data.Artikel;
import data.ArtikelData;

import java.util.List;
import java.util.function.Predicate;

public class PredicateDemo {
    public static <T> T firstMatch(List<T> candidates, Predicate<T> matchFunction) {
        for (T candidate : candidates) {
            if (matchFunction.test(candidate)) {
                return candidate;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        List<Artikel> lijst = ArtikelData.getArtikels();

        System.out.print("\nEerste artikel met 'L': ");
        //TODO

        System.out.print("\nEerste artikel met prijs > 500: ");
        //TODO
    }
}
